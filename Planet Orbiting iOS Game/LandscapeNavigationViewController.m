//
//  LandscapeNavigationViewController.m
//  Planet Orbiting iOS Game
//
//  Created by Clay Schubiner on 12/9/12.
//  Copyright (c) 2012 Stanford University. All rights reserved.
//

#import "LandscapeNavigationViewController.h"

@interface LandscapeNavigationViewController ()

@end

@implementation LandscapeNavigationViewController

-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskLandscape;
}

@end
