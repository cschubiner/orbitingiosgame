//
//  StoreLayer.h
//  Planet Orbiting iOS Game
//
//  Created by Alex Blickenstaff on 8/27/12.
//  Copyright (c) 2012 Stanford University. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface StoreLayer : CCLayer

+ (CCScene *) scene;

@end
