//
//  StoreItem.m
//  Planet Orbiting iOS Game
//
//  Created by Jeff Grimes on 7/22/12.
//  Copyright (c) 2012 Stanford University. All rights reserved.
//

#import "StoreItem.h"

// itemID numbers must be numbered as are items in an array - i.e. if there are 4 items in the store, then their ID numbers are 0, 1, 2, 3

@implementation StoreItem

@synthesize
title,
itemID,
description,
price;

@end