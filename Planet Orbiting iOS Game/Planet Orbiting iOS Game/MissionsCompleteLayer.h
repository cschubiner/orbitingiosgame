//
//  MissionsCompleteLayer.h
//  Planet Orbiting iOS Game
//
//  Created by Alex Blickenstaff on 8/28/12.
//  Copyright (c) 2012 Stanford University. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface MissionsCompleteLayer : CCLayer

+ (CCScene *) scene;

@end
